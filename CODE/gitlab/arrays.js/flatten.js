function flatten(elements){
    let arr = [];
    for(let i = 0; i < elements.length; i++){
        if(Array.isArray(elements[i])){
            arr = arr.concat(flatten(elements[i]));
        }
        else{
            arr.push(elements[i]);
        }
    }return arr;    
}

module.exports = flatten;
